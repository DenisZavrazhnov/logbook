package ru.DZavrzhnov.web.service;

import ru.DZavrzhnov.web.forms.PostForm;
import ru.DZavrzhnov.web.models.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {
    void addPost(PostForm postForm);

    List<Post> findAllByType(String type);

    Optional<Post> findById(Long id);

    void deleteById(Long id);

    List<Post> findAllByNumberOfLetters(String searchPattern);
}
