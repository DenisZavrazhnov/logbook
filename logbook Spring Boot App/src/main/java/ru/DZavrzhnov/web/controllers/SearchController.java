package ru.DZavrzhnov.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.DZavrzhnov.web.models.Post;
import ru.DZavrzhnov.web.service.PostService;

import java.util.List;


@RestController
public class SearchController {

    @Autowired
    private PostService postService;

    @GetMapping(value = "/search",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Post> searchPost(@RequestParam("sp") String searchPattern){
        return postService.findAllByNumberOfLetters(searchPattern);
    }
}
