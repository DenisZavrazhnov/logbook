package ru.DZavrzhnov.web.forms;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class PostForm {
    private String numberOfLetters;
    private String organization;
    private String person;
    private String dateOfCreate;
    private String dateOfIssue;
    @Email(message = "{errors.incorrect.email}")
    private String email;
    private String types;
}
